# Sichere Deine Daten
# Korrekturvorschlag: Halte deine Daten sicher

Wenn du ein Smartphone, Tablet oder einen Laptop dabei hast, trägst du ständig 
enorme Datenmengen mit dir herum: Wen du kennst, was du mit wem schreibst, 
deine persönlichen Dokumente und Fotos, die nicht nur von dir vertrauliche
Daten beinhalten können, sondern auch von dutzenden oder gar tausenden anderer
Menschen. Das sind aber nur Beispiele für Daten, die auf deinen digitalen
Geräten gespeichert sein können. Weil wir soviele Daten speichern und mit uns
herum tragen, kann es schwer sein, sie sicher zu halten - besonders, weil sie
dir relativ leicht weggenommen werden können. 

Deine Daten können an Grenzen beschlagnahmt, auf der Straße abgenommen oder bei
einem Einbruch erbeutet und innerhalb von Sekunden kopiert werden. Passwörter,
PINs und Sperren, die auf Gesten, zum Beispiel so genannten Wischcodes, können
die Daten auf deinen Geräten aber nicht sicher schützen. Es ist relativ leicht,
solche Sperren zu umgehen, weil die Daten in klar lesbarer Form auf dem Gerät
gespeichert werden. Ein:e [[../Glossar/AngreiferInnen|Angreifer:in]] muss nur direkt auf das Speichermedium
im Gerät zugreifen, um alle Daten ohne [[../Glossar/Passwort|Passwort]] auslesen und auswerten zu 
können.

Trotzdem *kannst* du es schwerer machen, nur mit physischem Zugriff an deine
Daten zu kommen. Hier findest du ein paar Möglichkeiten, deine Daten sicher zu
halten.

# Verschlüssele deine Daten
Wenn du deine Daten verschlüsselst, braucht ein:e Angreifer:in nicht nur dein
Gerät, sondern auch das Passwort, dass du für die Verschlüsselung benutzt. Es
macht am meisten Sinn, den kompletten Datenträger im Smartphone oder Laptop
zu verschlüsseln und nicht nur ein paar wenige Ordner. Die meisten Smartphones
und Computer haben eine Möglichkeit, ihre Datenträger vollständig zu 
verschlüsseln.
Für Smartphones und Tablets:
- Für *Android* findest du die Option immer unter FIXME"Security", oder wenn 
  du das Betriebssystem (neu) einrichtest.
- Bei *Apple* Geräten wie iPhones oder iPads findest du es als 
  FIXME"Data Protection" und kannst dort ein Passwort vergeben.
Für Computer:
- *Apple* bringt mit FileVault von Haus aus ein Feature mit, um ganze 
  Festplatten zu verschlüsseln.
- [[https://de.wikipedia.org/wiki/Linux-Distribution|Linux Distrobutionen]] erlauben üblicherweise bei der ersten Installation des 
  Betriebssystems Verschlüsselung für die komplette Festplatte einzurichten.
- Ab *Windows Vista* findest du mit BitLocker, was du brauchst, um die 
  Festplatte vollständig zu verschlüsseln.

<<< [exclamation-mark box]
Der Quellcode von Bitlocker ist nicht offen sondern geheim, was es schwer 
macht, unabhängig zu prüfen, wie sicher es tatsächlich ist. Wenn du BitLocker
nutzt, musst du Microsoft vertrauen, dass sie die Daten sicher und ohne
versteckte Schwachstellen speichern. Wenn du Windows nutzt, musst du aber genau
dieses Vertrauen ohnehin aufbringen. Wenn du Sorgen hast wegen Angriffen, in
denen du erwartest, dass Hintertüren in Bitlocker oder Windows eine Rolle 
spielen, überleg dir, ob du stattdessen ein quelloffenes [[../Glossar/Betriebssystem|Betriebssystem]] zu 
nutzen, etwa GNU/Linux oder BSD, besonders eine Variante, die gegen Angriffe
gehärtet ist, etwa Tails oder Qubes OS. Alternativ kannst du auch eine andere
Software nutzen, um deine Festplatte zu verschlüsseln, etwa [[https://www.veracrypt.fr/en/Home.html|Veracrypt]].
>>>

Was du dabei nicht vergessen solltest: Die Verschlüsselung kann nur höchstens
so sicher sein, wie dein Passwort. Wer dein Gerät hat, hat alle Zeit der Welt
um deine Passwörter herauszufinden. Eine effiktive Möglichkeit, einprägsame und
[[https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases|starke Passwörter zu erstellen]], ist es, [[https://www.eff.org/dice|Würfel]] und eine [[https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt|Wortliste]] zu nutzen um
Wörter zufällig auszuwählen. Gemeinsam ergeben diese Wörter dann eine 
[[../Glossar/Passphrase|"Passphrase"]]. Eine "Passphrase" ist ein besonders langes Passwort, um es 
dadurch sicherer zu machen. Für die Verschlüsselung von Festplatten empfehlen 
wir mindestens sechs Wörter. Für mehr Informationen kannst du unsere Anleitung
zum [[Sichere-Passwörter-erstellen|Erstellen von starken Passwörtern]] lesen.

Es kann dir aber schwer fallen, dir ein langes Passwort zu merken und es am
Smartphone einzugeben. Während Verschlüsselung ein hilfreiches Werkzeug sein 
kann, um Gelegenheitszugriffe zu verhindern, solltest du vertraulichere Daten
vor dem physischen Zugriff durch Gegner:innen schützen oder auf einem sehr viel
sichereren Gerät wegsperren.

# Ein Gerät absichern

Eine Umgebung sicher zu halten, kann schwer sein. Im leichtesten Fall musst du
Passwörter, Gewohnheiten und vielleicht die Software auf deinen Geräten 
anpassen. Im schlimmsten Fall musst du ständig hinterfragen, ob du gerade 
vertrauliche verlierst oder dich unsicherer Praktiken bedienst. Selbst, wenn du 
die Schwierigkeiten kennst, kann es sein, dass du nicht sicher kommunizieren 
kannst, weil die Personen, mit denen du kommunizieren musst sich nicht an 
im digitalen sichere Praktiken halten. Etwa, wenn jemand auf der Arbeit möchte,
dass du den Anhang einer Email von ihnen öffnest, obwohl deine Gegner:innen
sich als sie ausgeben könnten, um dir [[../Glossar/Schadware|Schadware]] zu schicken.

Wie sieht also die Lösung aus? Überlege dir, deine wertvollen [[../Glossar/Daten|Daten]] und
Unterhaltungen auf ein sichereres Gerät zu isolieren. Auf dem sicheren Gerät
kannst du deine Hauptversion deiner vertraulichen Daten halten. Nutze dieses 
Gerät nur wenn du musst und wenn du es benutzt, achte besonders darauf, was du
tust. Wenn du Anhänge öffnen oder unsichere Software nutzen musst, nutze ein
anderes Gerät.

<<< [lightbulb-box]
Ein gesonderter, sicherer Computer muss nicht so teuer sein, wie du vielleicht 
denkst. Ein Computer, den du nicht sehr oft nutzt und der nur wenig Software
installiert braucht, muss weder sonderlich schnell noch neu sein. Du kannst ein
älteres Netbook für einen Bruchteil dessen kaufen, was ein Smartphone oder 
Laptop neu kosten. Bei älteren Geräten hast du auch den Vorteil, dass du
bessere Chancen hast, dass sichere Software wie Tails darauf funktioniert als
bei neueren. Mancher Rat bleibt aber immer gleich: Wenn du ein Gerät oder 
[[../Glossar/Betriebssystem|Betriebssystem]] kaufst, halte es mit Softwareaktualisierungen auf einem 
aktuellen Stand. Aktualisierungen schließen oft Sicherheitslücken in älterem
Code, die für Angriffe ausgenutzt werden können. Achte auch darauf, dass dein
Betriebssystem neu genug ist, da manche ältere Betriebssysteme nicht mehr
unterstützt werden, nichtmal mit Sicherheitsupdates.
>>>

# Schritte, um einen sicheren Computer beim Aufsetzen abzusichern

1. Halte das Gerät gut versteckt und rede nicht darüber, wo es ist - ein Ort, 
   wo du merkst, wenn jemand dran gegangen ist, etwa ein abgeschlossener Schrank
2. [[../Glossar/Verschlüsselung|Verschlüssele]] die Festplatte deines Computers mit einer sicheren [[../Glossar/Passphrase|Passphrase]]. 
   So bleiben die Daten auf dem Computer unbrauchbar, falls das Gerät gestohlen 
   wird.
3. Setz ein Betriebssystem auf, das auf Privatssphäre und Sicherheit 
   ausgerichtet ist, etwa Tails. Vielleicht kannst (oder willst) du im Alltag
   kein Quelloffenes Betriebssystem nutzen, aber wenn du nur vertrauliche Emails
   oder Chatnachrichten von diesem sicheren Gerät lesen und bearbeiten willst,
   passt die Einstellung von Tails auf hohe Sicherheit.
4. Halte dein Gerät offline. Wenig überraschend ist der beste Weg, dich vor 
   Angriffen aus dem Internet zu schützen, dich nicht mit dem Internet zu 
   verbinden. Du kannst sicherstellen, dass sich das Gerät niemals mit dem
   lokalen Netzwerk oder WLAN verbindet und Daten nur mit physischen 
   Datenträgern wie CDs/DVDs, SD-Karten oder USB-Sticks. In Kreisen der Netz-
   werksicherheits wird von so einen Aufbau auch gesagt, dass eine  [[../Glossar/air-gap|"air gap"]]
   zwischen dem Computer und dem Rest der Welt. Während das eine extreme 
   Maßnahme ist, kann es eine Option sein für Daten, mit denen du nur selten
   arbeiten musst, die du aber niemals verlieren willst (etwa ein [[../Glossar/Verschlüsselungs-Schlüssel|Schlüssel]] für
   Verschlüsselung, eine Liste von Passwörtern oder eine Sicherungskopie von 
   Daten, die dir jemand anderes anvertraut hat). Meistens wird dir aber ein
   versteckter USB-[[../Glossar/Schlüssel|Schlüssel]] reichen, anstelle eines kompletten Computers.
   Ähnlich nützlich (oder nutzlos) wie ein Computer, der nicht ans Internet
   angeschlossen ist.
5. Melde dich nicht mit deinen üblichen Accounts an. Wenn du ein sicheres Gerät
   nutzt, um dich mit dem Internet zu verbinden, lege dir ein getrenntes Email
   Konto an für die Kommunikation von diesem Gerät und nutze [[https://www.torproject.org/de/|Tor]] (hier findest
   du Anleitungen für [[Anleitung-Tor-Linux|Linux]], [[Anleitung-Tor-MacOS|MacOS]] und [[Anleitung-Tor-Windows|Windows]]) um deine [[../Glossar/IP-Adresse|IP-Adresse]] versteckt
   zu halten von diesen Diensten. Wenn dich jemand gezielt mit Malware angreift
   oder nur deine Kommunikation überwacht können getrennte Konten zusammen mit
   Tor die Verbindung von dieser konkreten Maschine zu dir verschleiern.

<<< [exclamation mark box]
Während ein zusätzliches, sicheres Gerät helfen kann, wichtige, vertrauliche
Informationen vor Gegner:innen zu beschützen, schafft es auch ein offensicht-
liches Angriffsziel. Außerdem gibt es das [[../Glossar/Risikoabschätzung|Risiko]], die einzige Kopie deiner 
Daten zu verlieren, wenn die Maschine zerstört wird. Wenn deine [[../Glossar/Gegner:innen|Gegner:innen]]
ein Interesse daran haben, dass du alle deine Daten verlierst, halte sie nicht
an nur einer Stelle, egal wie sicher. Verschlüssele eine Kopie und lager sie an
einem anderen Ort.
>>>

Eine ähnliche Idee wie die einer sicheren Maschine ist die, einer unsicheren
Maschine: Ein Gerät, das du nur an gefährlichen Orten nutzt oder wenn du etwas
besonders gefährliches tust. Viele Aktivisti und Journalist:innen nehmen auf
Reisen zum Beispiel nur ein einfaches Notebook mit. Auf diesem Computer 
speichern sie keine ihrer üblichen Dokumente ab und nutzen sie ein anderes
Email-Konto als sonst, damit der Verlust möglichst gering ist, wenn er 
beschlagnahmt oder untersucht wird. Du kannst die gleiche Strategie auf
Mobildtelefone anwenden. Wenn du normalerweise ein Smartphone nutzt, überlege 
dir ein günstiges [[../Glossar/Burnerphone|Einweg-]] oder [[../Glossar/Burnerphone|Burnertelefon]] zu kaufen für bestimmte 
Kommunikation, wenn du unterwegs bist.

