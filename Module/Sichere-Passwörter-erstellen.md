# Sichere Passwörter erstellen
## Sichere Passwörter erstellen mit Hilfe eines Passwortmanagers

Passwörter mehrfach zu verwenden ist eine besonders schlechte Sicherheitspraktik
Wenn jemand mit bösen Absichten ein Passwort in die Finger kriegt, das Du für
mehrere Dienste benutzt, kann mit diesem einen verlorenen Passwort Zugang zu
vielen Diensten auf einmal erlangt werden. Das ist, warum es so wichtig ist,
mehrere, starke, einzigartige Passwörter zu benutzen.

Glücklicherweise können [[../Glossar/Passwortmanager|Passwortmanager]] helfen. Ein Passwortmanager ist ein 
Werkzeug um Passwörter zu erzeugen und zu speichern. So kannst Du bei 
vielen unterschiedlichen Diensten jeweils unterschiedliche Passwörter nutzen, 
ohne sie dir alle merken zu müssen. Passwortmanager:
- erzeugen starke Passwörter, die von Menschen schwer zu erraten sind.
- speichern sicher viele Passwörter (und Antworten auf Sicherheitsfragen).
- schützen alle gespeicherten Passwörter mit einem einzelnen [[../Glossar/Hauptpasswort|Hauptpasswort]]
  (oder [[../Glossar/Passphrase|Passphrase]])

Ein Beispiel für einen Passwortmanager ist KeePassXC. Es ist quelloffen und 
kostenlos. Du kannst ihn auf deinem Desktop installieren und in deinen 
[[../Glossar/Webbrowser|Webbrowser]] integrieren. KeePassXC speichert Änderungen nicht sofort. Das heißt,
wenn KeePassXC abstürzt nachdem Du einige Passwörter hinzugefügt hast, kannst
Du sie für immer verlieren. Du kannst dieses Verhalten in den Einstellungen 
ändern.

<<< [exclamationmark-box]
Fragst du dich, ob ein Passwortmanager das richtige Werkzeug für dich ist? Wenn
Du eine:n mächtige:n [[../Glossar/Gegner_in|Gegner:in]], wie eine Regierung, hast, kann es sein, dass er
das nicht ist.
>>>

Beachte:
- einen Passwortmanager zu nutzen, erzeugt einen einzelnen Punkt, von dem alles
  abhängt.
- Passwortmanager sind ein offensichtliches Ziel für Gegner:innen.
- Forschungsergebnisse deuten darauf hin, dass viele Passwortmanager 
  Schwachstellen haben.

Wenn aufwändige digitale Angriffe zu deinen Sorgen gehören, kann etwas 
technisch einfacheres besser sein. Du kannst starke Passwörter händisch 
erzeugen (siehe "Sichere Passwörter mit Würfeln erstellen" weiter unten) und
es aufgeschrieben immer direkt bei dir haben.

<<<[lightbulb-box]
/Moment, sollen wir Passwörter nicht nur in unserem Gedächtnis behalten und
niemals aufschreiben?/ Tatsächlich ist es hilfreich, das Passwort 
aufzuschreiben und in den Geldbeutel zu stecken. So weißt du wenigstens, wenn 
Du deinaufgeschriebenes Passwort verlierst oder es gestohlen wird.
>>>

## Sichere Passwörter mit Würfeln erstellen

Es gibt ein paar Passwörter, die Du Dir einprägen solltest und die besonders 
stark sein sollten. Diese beinhalten:
- Passwörter um dich an deinem Gerät anzumelden
- Passwörter für Verschlüsselung (wie z.B. Festplattenverschlüsselung)
- das [[../Glossar/Hauptpasswort|Hauptpasswort]], oder [[../Glossar/Passphrase|Passphrase]], deines [[../Glossar/Passwortmanager|Passwortmanagers]]
- dein Email-[[../Glossar/Passwort|Passwort]]

Eine von vielen Schwierigkeiten ist, dass wenn Menschen sich Passwörter selbst
aussuchen, sie [[http://people.ischool.berkeley.edu/~nick/aaronson-oracle/|nicht sehr gut darin sind]], unvorhersehbare Entscheidungen zu 
treffen. Eine effiktive Möglichkeit [[https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases|starke und einprägsame Passwörter]] zu 
erstellen ist es, [[https://www.eff.org/dice|Würfel]] und eine [[https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt|Wortliste]] zu nutzen um zufällig Wörter 
auszuwählen. Zusammen ergeben diese Wörter Deine "Passphrase". Eine "Passphrase" 
ist eine Art von Passwort, das länger ist für zusätzliche Sicherheit. Für 
Festplattenverschlüsselung und deinen Passwortmanager empfehlen wir mindestens
sechs Wörter zu verwenden.

/Warum mindestens sechs Wörter nutzen? Warum Würfel nutzen um die Wörter 
zufällig auszuwählen?/ Je länger und zufälliger ein Passwort, desto schwerer
wird es sowohl für Computer als auch Menschen es zu erraten. Um herauszufinden,
warum du ein so langes und schwer zu erratendes Passwort brauchst, ist [[animierte-uebersicht-wie-du-ein-supersicheres-passwort-mit-wuerfeln-erstellst|hier]]
ein Video mit der Erklärung.

<<< [lightbulb-box]
Probiere eine der [[https://www.eff.org/dice|Wortlisten]] der EFF um eine Passphrase zu generieren.
>>>

## Ein Wort zu "Sicherheitsfragen"
Nimm dich in Acht vor, wenn Websiten "Sicherheitsfragen" nutzen, um zu 
bestätigen, wer du bist. Häufig können hinreichend motivierte [[../Glossar/AngreiferInnen|Angreifer:innen]]
ehrliche Antworten auf die Fragen aus öffentlich findbaren Quellen lernen.
Die können sie dann nutzen, um dein [[../Glossar/Passwort|Passwort]] komplett zu umgehen.

Gib statt dessen ausgedachte Antworten, die nur du weißt. Wenn dich die 
[[../Glossar/Sicherheitsfrage|Sicherheitsfrage]] etwa fragt:
"Was war der Name deines ersten Haustiers?"
Dann kann deine Antwort ein zufälliges Passwort sein, das dein [[../Glossar/Passwortmanager|Passwortmanager]]
generiert hat.

<<< [lightbulb-box]
Überleg, bei welchen Seiten du Sicherheitsfragen beantwortet hast und denk
darüber nach, die Antworten zu ändern. Achte darauf kein Passwort und keine
Antwort auf eine Sicherheitsfrage an verschiedenen Accounts zu nutzen.
>>>

## Passwörter auf mehreren Geräten aktuell halten
Viele [[../Glossar/Passwort|Passwort]]manager erlauben dir, deine Passwörter über mehrere Geräte zu
synchronisieren. Damit kannst du ein Passwort auf einem Gerät aktualisieren,
und alle anderen Geräte machen diese Änderung nach.

Passwortmanager können Passwörter "in der Cloud" speichern, also verschlüsselt
auf einem Server, der irgendwo anders steht. Wenn du dein Passwort brauchst,
werden diese Passwortmanager das Passwort automatisch herunterladen und
entschlüsseln. Passwortmanager, die ihre eigenen Server nutzen, um die 
Passwörter zu speichern sind bequemer, aber auch anfälliger für Angriffe.
Wenn deine Passwörter sowohl auf deinem Computer als auch auf einem Server 
gespeichert sind, müssen Angreifer:innen nicht deinen Computer übernehmen, um
an deine Passwörter zu kommen. (Allerdings müssen sie das [[../Glossar/Passwort|Passwort]] deines
[[../Glossar/Passwortmanagers|Passwortmanagers]] knacken.)

<<< [lightbulb-box]
Um sicher zu gehen, behalte ein Backup deiner Passwort-Datenbank. Ein Backup 
ist hilfreich, falls deine Passwortdatenbank in einem Absturz verloren geht 
oder dir dein Gerät weggenommen wird. Normalerweise können Passwortmanager
Backups anfertigen, oder du nutzt ein übliches Backupprogramm.
>>>

## Mehr-Faktoren-Authentifizierung und Einmalpasswörter
Starke, einzigartige Passwörter machen es sehr viel schwerer für 
Angreifer:innen, auf deine Konten zuzugreifen. Um sie noch weiter abzusichern,
kannst du [[../Glossar/2FA|2-Faktor-Authentifizierung]] aktivieren.

Manche Dienste bieten 2-Faktor-Authentifizierung (auch 2FA oder 
Mehrfaktorauthentifizierung genannt), die für den Login eine zweite Komponente
verlangen. Dieser zweite Faktor kann zum Beispiel ein Code sein, der aus einer 
Liste möglicher Codes nur einmal gewählt wird (sog. TAN-Listen) oder jedes mal
neu und einzigartig generiert wird, z.B. von einem Programm auf einem mobilen
Gerät.

2FA mit Hilfe eines Telefons kann unterschiedlich passieren:
- Dein Telefon kann ein spezielles Program installiert haben, das solche Codes 
  erzeugt (zum Beispiel Google Authenticator oder Authy) oder du kannst ein 
  extra Gerät verwenden (z.B. ein YubiKey); oder
- Der Dienst kann dir eine SMS oder Textnachricht mit dem Sicherheitscode 
  schicken, wenn du dich einloggen willst.

<<< [lightbulb-box]
Wenn du die Wahl hast, entscheide dich für ein Programm, das du ausführst, oder
ein Gerät bei dir, weil es leichter ist, einen Code umzuleiten, als deine App
zu umgehen.
>>>

Manche Dienst, etwa Google, erlauben es, eine Liste mit einmal-Passwörtern zu
erzeugen. Diese sind dazu gedacht ausgedruckt oder anders auf Papier kopiert zu
werden, das du dann mit dir führst. Jedes dieser Passwörter funktioniert nur 
einmal, sodass Spionagesoftware, die eines mitlauscht, wenn du es eingibst, es 
nicht benutzen kann.

## Manchmal musst du dein Passwort verraten
Unterschiedliche Orte haben unterschiedliche Gesetze dazu, wann du ein Passwort
weitergeben musst. In manchen Juristiktionen kannst du dich juristisch dagegen
wehren, wenn von dir verlangt wird, dein Passwort zu verraten. In anderen gibt
es lokale Gesetze, die es dem Staat erlauben, es von dir zu verlangen und dich
sogar einzusperren, wenn sie glauben, dass du einen Schlüssel oder ein Passwort
hast. Androhungen körperlicher Gewalt können benutzt werden um dich dazu zu 
zwingen, die Geheimheit deines Passworts aufzugeben. Es gibt auch Situationen,
etwa, wenn du über eine Grenze hinweg reisen willst, in der Behörden dich 
länger hinhalten, oder deine Geräte beschlagnahmen können, wenn du dich 
weigerst, deine Passwort zu verraten oder dein Gerät zu entsperren.

Wir haben eine eigene Anleitung zum übertreten der US-amerikanischen Grenze,
die Tipps gibt, wie du damit umgehen kannst, wenn Zugriff auf deine Geräte 
verlangt wird während du in oder aus den Vereinigten Staaten von Amerika reist.
In anderen Situationen solltest du darüber nachdenken, wie dich jemand dazu 
zwingen könnte, dein Passwort herzugeben und was die Konsequenzen davon wären.
