# Warum Metadaten wichtig sind
[[../Glossar/metadaten|Metadaten]] werden oft beschrieben als alles, außer dem
Inhalt deiner Gespräche. Du kannst dir Metadaten als das digitale Äquivalent 
eines Umschlags vorstellen. Und genau wie ein Umschlag Informationen enthält
über den:die Absender:in, Empfänger:in und das Ziel der Nachricht, ist es bei
Metadaten. Metadaten sind Informationen über die digitale Unterhaltungen, die
verschickst und empfängst. Ein paar Beispiele, was Metadaten enthalten:
* Den Betreff deiner Emails
* Die Länge deiner Nachrichten und Unterhaltungen
* Der Zeitpunkt deiner Unterhaltung
* Dein Aufenthaltsort bei der Unterhaltung
* Die Beteiligten deiner Unterhaltung
[[https://ssd.eff.org/files/2018/11/30/endtoendencryptionmetadata-nolocks.png]]
Aufgrund der Geschichte genießen Metadaten in manchen Ländern - einschließlich 
der USA - einen geringeren rechtlichen Schutz, als der Inhalt der Unterhaltung.
Die Polizei darf in vielen Ländern leichter Aufzeichnungen einholen, mit wem du 
in den letzten drei Monaten telefoniert hast, als deine Leitung abzuhören um
mitzuhören, was du tatsächlich sagst.
Diejenigen, die Metadaten sammeln oder Zugang verlangen, wie zum Beispiel 
Regierungen oder Telefongesellschaften, argumentieren, dass die Offenlegung
(und Sammlung) von Metadaten nichts Besonderes ist. Unglücklicherweise 
[[http://www.nybooks.com/blogs/nyrblog/2014/may/10/we-kill-people-based-metadata/|stimmt das einfach nicht]]. Selbst kleine Stichproben an Metadaten können einen 
intimen Blick ins Leben einer Person erlauben. Lass uns einen Blick darauf
werfen, wie aufschlussreich Metadaten tatsächlich sein können für die
Regierungen und Unternehmen, die sie sammeln:
* Sie wissen, dass du um 2:24 Uhr eine Telefonsexhotline angerufen hast und das 
  Gespräch 18 Minuten dauerte. Aber sie wissen nicht, worüber ihr geredet habt.
* Sie wissen, dass du von der Golden Gate Bridge aus eine Suizidhilfehotline 
  angerufen hast. Das Thema des Anrufs bleibt ein Geheimnis.
* Sie wissen, dass du eine Email von einem HIV Testlabor erhalten hast, und
  dann innerhalb einer Stunde deine:n Arzt:in angerufen hast und die Website 
  einer HIV Hilfsgruppe aufgerufen hast. Aber sie wissen nicht, worum es in 
  der Email oder dem Telefonat ging.
* Sie wissen, dass du von einer Gruppe von Aktivisti für digitale Rechte eine 
  Email gekriegt hast mit dem Betreff "Lass uns dem Congress sagen: Stoppt 
  SESTA/FOSTA" und sofort deine:n Abgeordnete:n angerufen hast. Aber der Inhalt
  dieser Unterhaltungen bleibt sicher vor staatlichen Schnüffeleien.
* Sie wissen, dass du deine:n Gynokolog:in angerufen hast, das Gespräch eine
  eine halbe Stunde dauerte und du später im Laufe des Tages die Nummer der
  lokalen Abtreibungsklinik angerufen hast.
Es kann schwer sein, deine Metadaten davor zu schützen, gesammelt zu werden,
weil oft Dritte diese Metadaten benötigen, um dich erfolgreich kommunizieren
zu lassen. Genau wie Briefträger:innen den Umschlag deines Briefs lesen müssen,
um ihn zuzustellen, muss digitale Kommunikation oft mit Quelle und Ziel 
markiert sein. Mobiltelefonprovider müssen ungefähr wissen, wo du bist, um 
Anrufe für dich dorthin zu leiten.
Dienste wie Tor versuchen die Menge an Metadaten, die bei üblicher 
Onlinekommunikation anfallen. Bis Gesetze aktualisiert werden um besser mit
Metadaten umzugehen und die Werkzeuge sie zu minimieren weiter verbreitet sind,
ist das Beste, was du tun kannst, dir bewusst zu sein, was für Metadaten mit 
deiner Kommunikation verbunden sind, wer Zugriff darauf hat und wie sie genuzt
werden könnten.
