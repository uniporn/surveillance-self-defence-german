# Animierte Übersicht: Schütze dein Gerät vor Hackern
Bösartige Angreifer können dein Gerät übernehmen - sei es ein Laptop, Desktop, 
Smartphone oder ein Tablet - und es nutzen, um mehr über dich herauszufinden,
dich auszuspionieren oder Beweismaterial zu platzieren. Diese Animation erklärt
ein paar Wege, auf denen dein Computer übernommen werden (auch bekannt z.B. als
"pwned", "0wned") kann, und wie du dich davor schützen kannst.
1. Ein Emailanhang mit [[../Glossar/schadsoftware|Schadsoftware]]: Du kannst dazu getäuscht werden, ein 
Programm auszuführen, indem du [[phishing-vermeiden|einen Emailanhang öffnest]], der harmlos aussieht,
aber tatsächlich Schadsoftware enthält. Schadsoftware kann auf deinem Computer 
das Mikrofon einschalten und deine Gespräche übertragen, deinen Bildschirm 
aufnehmen, speichern, was du tippst, Dateien kopieren oder sogar falsche Daten
ablegen. Sei daher sehr vorsichtig damit, seltsame Emailanhänge zu öffnen und 
frag lieber die Person, von der du den Anhang erhalten hast, wenn du dir 
nicht sicher bist.
2. Ein bösartiger Weblink: Es ist möglich, computer aus der Ferne mit 
Schadsoftware zu infizieren. Dafür musst du nur eine Webseite aufrufen (auch
"Drive-by-download" genannt). Wenn ein Link dich auffordert, etwas zu 
installieren, stimme dem nicht zu. Und wenn dein [[../Glossar/webbrowser|Webbrowser]] oder Suchmaschine 
warnt, eine Seite könnte bösartig sein, klick den Zurück-Button.
3. USB- oder Thunderbolt Anschlüsse: Angreifer können ein bösartiges Programm 
auf deinen Computer kopieren, oder auch anders deinen Computer übernehmen, 
wenn sie etwas an deinem USB- oder Thunderboltanschluss anschließen. Das 
gleiche gilt für CDs und DVDs. Manchmal ist dort ein Programm enthalten 
(autorun.exe), das automatisch ausgeführt wird, sodass dein Computer sofort 
infiziert wird, sobald du die CD einlegst. Sei daher sorgsam, was du an deinen 
Computer ansteckst, und wenn du sicher gehen willst, gib keinen Fremden Zugang 
zu deinem Gerät.

Erinnere dich daran, dass moderne Computer dafür designed sind, solche Angriffe 
abzuwehren. Solange wir sie so bauen, wirst du eine Möglichkeit haben, dich
dagegen zu wehren, 0wned zu werden. Für Tipps wie du Angriffe auf deinen
Computer erkennst, lies unseren Ratgeber zu [[malware|Malware]].
