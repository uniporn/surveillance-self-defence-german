# Animierte Übersicht: Wie du ein supersicheres Passwort mithilfe von Würfeln erstellst
Computerpasswörter haben die Möglichkeit, große Schätze an Informationen zu 
heben, aber manche Passwörter sind so einfach, dass es Dieben leicht fällt, 
sie richtig zu raten. Eine häufige Erfolgsmethode ist, bekannte Passwörter 
(z.B. "ichliebedich") zu vermeiden und Sonderzeichen oder Zahlen hinzuzufügen, 
um das Passwort schwerer erratbar zu machen. Allerdings verwenden Angreifer 
heutzutage Computerprogramme, um Passwörter zu erraten - und diese Programme
können /Millionen/ von Passwörtern jede Sekunde versuchen. Um vor solchen 
Angriffen sicher zu sein, muss ein [[../Glossar/Passwort|Passwort]] lang und zufällig sein. Aber 
lange, zufällige Passwörter sind für Menschen unfassbar schwer zu merken.

Um ein tatsächlich zufälliges Passwort zu entwickeln, das du dir aber auch 
merken kannst, kannst du Arnold G. Reinholds Würfeltechnik probieren - eine
Methode, mithilfe von Würfeln und einer Liste von [[https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt|Würfelwarewörtern]]
Das sich ergebende Passwort sollte zufällig genug sein, um gegen 
automatisierte Angriffe (außer jene von großen Organisationen mit vielen 
Resourcen, wie z.B. der NSA oder anderen Spionagediensten). Denk immer daran,
dein starkes Passwort geheim zu halten und vermeide es, deine Passwörter 
mehrfach zu verwenden. Abhängig davon, wieviele Wörter dein Passwort enthält, 
kannst du sogar in der Lage sein, die fortgeschrittensten Angriffe vereiteln.
Fünf zufällige Wörter (64 bits) sollen gegen Kriminelle schützen, während 
sechs zufällige Wörter (77 bits) gegen alle, außer die motiviertesten
Angreifer auf Landesebene (wi z.B. die NSA), helfen sollen.

Diese Animation enthält eine spezifische Anleitung zur Nutzung von Würfelware;
Mehr zum Management vieler Passwörter findest du in diesen Anleitungen:
* [[../node/85|Nutzung von Passwortmanagern um online sicher zu bleiben]]
* [[../node/52|Anleitung zur Nutzung von KeepassX]]
* [[../node/23|Erstellung starker Passwörter]]

* [[../tag/animationen|Animationen]]
