# Animierte Übersicht: Nutzung eines Passwortmanagers, um online sicher zu gehen
Du machst viel über das Internet und das bedeutet, dass du vemrutlich viele
Accounts auf vielen verschiedenen Webseiten hast. Aber wusstest du, dass du,
wenn du das selbe [[../Glossar/passwort|Passwort]] auf jeder dieser Webseiten oder *fast* das selbe 
Passwort, das du für jede Seite nur wenig änderst, du größere Gefahr läufst,
von einem [[../Glossar/angriff|Cyberangriffs]] betroffen zu sein? Weil wenn in eine dieser Webseiten 
eingebrochen wird, können Diebe an die Passwörter aller kommen, die diese Seite
genutzt haben. Und die so gewonnnen Passwörter können sie dann auch nutzen, um
auf anderen Seiten Zugang zu Accounts zu gewinnen.

Solche Einbrüche passieren häufiger, als du denkst - häufig sogar ohne, dass
die Betreiber:innen etwas davon mitkriegen. Um der Versuchung, Passwörter
wiederzuverwenden, zu widerstehen, kannst du folgendes versuchen:
* Schreibe deine Passwörter auf: Warte - soll ich nicht mein Passwort nur in
meinem Kopf haben und niemals aufschreiben? Nun, wenn du sie aufschreibst und
an einem sicheren Ort wie deinem Geldbeutel aufbewahrst, weißt du wenigstens,
wenn dein aufgeschriebens Passwort verloren geht oder gestohlen wird.
* Nutze einen [[../Glossar/passwortmanager|Passwortmanager]]: Passwortmanager sind Programme, die du 
runterladen kannst, die dir einzigartige Passwörter generieren, speichern und 
sogar das jeweils passende in die meisten Webseiten und anderen Onlinedienste 
eintragen kann. Wenn du einen Passwortmanager installierst, solltest alle 
alten und schwachen Passwörter austauschen. Passwortmanager können deine
Logindaten sicher halten und zwischen deinen Geräten synchronisieren.
Warnhinweis: Passwortmanager benötigen ein [[../Glossar/master-passwort|Masterpasswort]] - eines, das du in 
den Passwortmanager eintippst, um alle anderen zu entsperren. Dieses Passwort
wirst du besonders sicher, aber leicht zu erinnern, wählen wollen, weil es das
eine [[../Glossar/passwort|Passwort]] sein wird, dass du nicht im Passwortmanager speichern kannst.

Für weitere Informationen, schau dir unseren Leitfaden zur 
[[node/23|Erstellung starker Passwörter]] an
