# Zur digitalen Sicherheit in sieben Schritten
Hier sind ein paar grundlegende Tipps, die du beachten solltest, wenn du dir
über deine eigene digitale Sicherheit Gedanken machst.
## Wissen ist Macht
Gute Sicherheitsentscheidungen fangen damit an, dass du das notwengige Wissen 
über deine Lage hast. Einen guten Anfang stellen daher die folgenden Fragen 
dar:
* Was möchte ich schützen?
* Vor wem möchte ich es schützen?
* Wie wahrscheinlich ist, dass ich es beschützen muss?
* Was für Konsequenzen hat es, wenn der Schutz versagt und wie schlimm sind 
sie für mich?
* Wieviel Aufwand bin ich bereit, in die Verhinderung möglicher Konsequenzen zu
investieren?

Wenn du diese Fragen beantwortet hast, kanst du deine digitalen 
Sicherheitsbedürfnisse besser einschätzen und [[dein-sicherheits-plan|einen Sicherheitsplan]] oder dein
[[../Glossar/threat-model|Threat Model]]. Du bist schon weiter, als du denkst!

## Das schwächste Glied
Wie in dem Sprichwort gilt auch bei Sicherheit, dass die Kette nur so stark 
ist, wie das schwächste Glied. Das beste Türschloss bringt etwa nichts, wenn
am Fensterriegel gespart wurde. Genauso wirst du die Vertraulichkeit einer 
Email nicht durch Verschlüsselung schützen können, wenn eine unverschlüsselte 
Kopie auf deinem Laptop liegt und der gestohlen wird. Denk über alle 
Informationen über dich und alle deine Geräte nach und finde das schwächste
Glied in deinen digitalen Sicherheitspraktiken.

## Einfacher ist sicherer und leichter zu benutzen
Manche Menschen werden von jeder glänzenden, neuen Sicherheitslösung, von der
sie hören in Versuchung geführt. Das führt aber nur dazu, dass sie soviele 
Werkzeuge nutzen, dass sie nicht alle richtig bedienen können. Komplexe 
Systeme machen es uns schwerer, das schwächste Glied zu erkennen. Halte es 
daher übersichtlich. Manchmal kann die sicherste Möglichkeit die am wenigsten 
technische sein. Computer können viele tolle Dinge, aber manchmal sind die 
Sicherheitsprobleme von einem Stift und einem Notizbuch leichter zu erfassen 
und damit auch zu bewältigen.

## Teuer heißt nicht gut
Glaube nicht, dass die teuerste Lösung die Beste ist. Besonders, wenn sie 
Resourcen kostet, die woanders dringend benötigt werden. Kostengünstige 
Maßnahmen wie den Müll zu schreddern, bevor er am Straßenrand landet, kann 
deinem Sicherheitskonzept ordentlich Wums verleihen.

## Vertrauen ist nicht an und für sich falsch (Sei dir aber immer im klaren darüber, wo deines liegt)
Tipps zur Computersicherheit klingen schnell so, als solltest du niemandem
außer dir selbst auch nur einen Hauch weit trauen. Im echten Leben vertraust
du aber vielen Menschen mit zumindest einem Teil der Informationen über dich,
von deiner engsten Familie und Freunden, über deine:n Ärtzt:in oder Anwält:in.
Was im digitalten Kontext schwierig wird, ist zu verstehen, wem du vertraust,
und mit was. Du könntest deinen Anwält:innen eine Liste mit Passwörtern geben,
aber hast du über die Möglichkeiten nachgedacht, die ihnen das gibt? Oder wie 
es einem Angreifer eventuell dadurch leichter fällt, an deine Passwörter zu 
kommen? Du könntest Dokumente bei einem Internetdienst wie Dropbox oder Google 
ablegen, die nur für dich bestimmt sind, aber du lässt auch Dropbox und Google 
den Zugang dazu. Online wie offline - je weniger Menschen dein Geheimnis 
kennen, desto größer ist deine Chance, es geheim zu halten.

## Es gibt nicht "den einen" Sicherheitsplan
Erstell dir einen Sicherheitsplan, der für dich funktioniert und für die 
Risiken, denen du ausgesetzt bist, und den Möglichkeiten, die dir und deinem 
Umfeld zur Verfügung stehen. Ein auf dem Papier perfekter Plan wird nicht 
funktionieren, wenn er im Alltag zu schwer umzusetzen ist.

## Was heute sicher ist, muss nicht morgen immer noch sicher sein
Es ist essentiell, deine Maßnahmen konstant zu überprüfen. Nur, weil sie 
letztes Jahr oder letzte Woche sicher waren, heißt das noch lange nicht, dass
sich dich heute immer noch genau so gut schützen. Schau auch weiterhin auf 
Seiten wie diese, weil die Welt um uns und unser Verständnis von ihr entwickeln
sich weiter und wir passen unsere Inhalte daran an. Vergiss nicht: Sicherheit
ist ein andauernder Prozess.

